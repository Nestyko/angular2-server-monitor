import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AuthService } from './auth.service';
import { SailsService } from 'angular2-sails';
import { MaterializeModule } from 'angular2-materialize';

//Routes
import { routing, appRoutingProviders } from './app.routing';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SensorDetailComponent } from './sensor-detail/sensor-detail.component';
import { ThingDetailComponent } from './thing-detail/thing-detail.component';
import { ThingListComponent } from './thing-list/thing-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SensorDetailComponent,
    ThingDetailComponent,
    ThingListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterializeModule,
    routing
  ],
  providers: [
    AuthService,
    SailsService,
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
