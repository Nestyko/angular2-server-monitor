import { Routes, RouterModule }   from '@angular/router';

import { ModuleWithProviders } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { ThingListComponent } from './thing-list/thing-list.component';

const appRoutes: Routes = [
    { path: '', component: LoginComponent },
    {
        path: 'things',
        component: ThingListComponent
    }
]

export const appRoutingProviders: any[] = [

]

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes)