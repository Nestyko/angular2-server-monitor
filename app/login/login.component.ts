import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  isAuth: boolean;

  constructor(
    private _authService: AuthService,
    private _router: Router) { }

  ngOnInit() {
    this._authService.isAuth().subscribe(
      ok => {
        if(ok){
          console.log('logged in', ok);
          this._router.navigate(['/things']);
        }
      }
    )

  }

  login(){
    this._authService.login(this.email, this.password);
  }

  


}
