import { Injectable } from '@angular/core';
import { SailsService } from './sails.service';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { SERVER_URL } from './server-url';

@Injectable()
export class AuthService {

  private isAuthenticated: Subject<boolean> = new BehaviorSubject<boolean>(false);
  jwt: string = '';
  expires: number = 0;
  error: any;
  email: string = '';

  constructor(private _sailsService: SailsService) { 
    
    if(this.jwt !== ''){
      this.isAuthenticated.next(true);
      debugger;
      io.sails['headers'] = {
        access_token : this.jwt
      }
    }else{
      this.isAuthenticated.next(false);
    }
    this._sailsService.connect(SERVER_URL);
   }

  login(email: string, password: string): void{
    let user = {
      email: email,
      password: password
    }
    this._sailsService.post('/auth/login', user)
      .subscribe(
        resData => {
          this._sailsService.get('/user/jwt').subscribe(
            data => {
              //Save this in localstorage
              this.jwt = data.token;
              this.isAuthenticated.next(true);
              this.expires = data.expires;
            },
            error => {
              console.error('error getting the jwt', error);
            },
            () => {
              console.log('login completed');
            }
          )
        },
        error => {
          this.isAuthenticated.next(false);
          this.error = error;
        }
      )
  }

  isAuth(){
    return this.isAuthenticated.asObservable();
  }

  logout(){
    this._sailsService.get('/auth/logout')
      .subscribe(
        resData => {
          this.isAuthenticated.next(false);
        }
      )
  }


}
