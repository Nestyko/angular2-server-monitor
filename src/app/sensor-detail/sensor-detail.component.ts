import { Component, OnInit, Input } from '@angular/core';

import { ThingService } from '../thing.service';
import { Sensor } from '../sensor';

@Component({
  selector: 'app-sensor-detail',
  templateUrl: './sensor-detail.component.html',
  styleUrls: ['./sensor-detail.component.css']
})
export class SensorDetailComponent implements OnInit {


  @Input()
  sensor: Sensor;

  percentage: string;

  constructor(
    private _thingService: ThingService) { }

  ngOnInit() {
    this.percentage = "0%";
    this._thingService.getSensor(this.sensor.id)
      .subscribe(
        sensor => {
          this.sensor = sensor;
          this.percentage = sensor.lastMeasure.value + "%";
        }
      )
  }


}
