import { Injectable } from '@angular/core';
import { SailsService } from './sails.service';
//import { SailsEventService } from './sails-event.service';
import { Subject, BehaviorSubject } from 'rxjs';
import { Thing } from './thing';
import { Sensor } from './sensor';
import { Observer, Observable } from 'rxjs';
declare let io: any;

/**
 * TODO: 
 * Things observable of array
 * Sensor individual observables
 */

@Injectable()
export class ThingService {

  private things$: Subject<Thing[]> = new BehaviorSubject<Thing[]>(null);
  private things : Thing[];
  //sensors$: Subject<Array<Sensor>> = new BehaviorSubject<Array<Sensor>>(null);
  sensors$: Subject<any> = new BehaviorSubject<any>(null);
  sensors: any = [];
  
  private sensor$: Subject<Sensor> = new Subject<Sensor>();
  
  constructor(
    private _sailsService: SailsService) { 
      //this._sailsService.connect('http://localhost:1337');
      console.log('getting the things');
      this.getAllThings(/*Add the user*/);

      


    }

  private startListeners(){
    console.log('starting to listen');
    this._sailsService.on('thing').subscribe(
        (resData) => {
        let verb = resData.verb;
        let obj = resData.data;
        if (verb == 'created') {
            this.things.push(obj);
            this.things$.next(this.things);
            return;
        }
        if (verb === 'updated') {
          this.things = this.things.map(
            o => {
              let data = resData.data;
              if (o.id === data.id) {
                for (let prop in data) {
                  if (data.hasOwnProperty(prop)) {
                    o[prop] = data[prop];
                  }
                }
              }
              return o;
            }
          )
          this.things$.next(this.things);
          return;
        }
        if (verb === 'destroyed') {
          this.things = this.things.filter(t => t.id !== resData.id);
          return;
        }
      },
      (error) => {
        console.error('Error on SailsEvent', error);
      },
      () => {
        console.log('complete subscription to "thing" event');
      }
      )
      var sensors = this.sensors;
      /*
      io.socket.on('sensor', resData => {
        let verb = resData.verb;
          switch(verb){
            case 'addedTo':{
              if(resData.attribute == 'measures'){
                console.log(sensors);
                let sensor = sensors.filter(sensor => sensor.id === resData.id)[0];
                this._sailsService.get('/measure/'+resData.addedId).subscribe(
                  measure => {
                    delete measure.sensor;
                    sensor.lastMeasure = measure;
                    this.sensor$.next(sensor);
                  }
                )
              }
            }
              console.log(resData);
          }
      });*/
      
      this._sailsService.on('sensor').subscribe(
        resData => {
          let verb = resData.verb;
          switch(verb){
            case 'addedTo':{
              if(resData.attribute == 'measures'){
                console.log(sensors);
                let sensor = this.sensors.filter(sensor => sensor.id === resData.id)[0];
                this._sailsService.get('/measure/'+resData.addedId).subscribe(
                  measure => {
                    delete measure.sensor;
                    sensor.lastMeasure = measure;
                    this.sensor$.next(sensor);
                  }
                )
              }
              break;
            }
            case 'updated':
              console.log('sensor ALERT ****************');
              console.log(resData);
              let sensorId = resData.id;
              if(typeof resData.data.alert !== 'undefined'){
                let sensor = this.sensors.filter(s => s.id === sensorId)[0];
                sensor.alert = resData.data.alert;
                this.sensor$.next(sensor);
              }
              break;
          }
        }
      )
  }



  private addLastMeasure(array: Thing[]) {
    array.map(thing => {
      thing.sensors.map(sensor => {
        /*this._sailsService
          .get('/sensor/'+sensor.id+'/measures?limit=1&sort=updatedAt DESC')*/
        //this._sensorService.get(sensor.id);
      })
    })
  }

  getThings() {
    return this.things$.asObservable();
  }

  private getAllThings(user? : string){
    let url = '/thing';
    if(user){
      url += '?user='+user;
    }
    this._sailsService.get(url).subscribe(
      things => {
        this.things = things;
        things.map(
          thing => {
            thing.sensors.map(
              sensor => {
                this.sensors.push(sensor);
              }
            )
          }
        )
        console.log('sensors fetched', this.sensors);
        this.things$.next(things);

        this.loadUnits().subscribe(
          sensor => {
            this.sensor$.next(sensor);
          }
        )

        //this._sailsEventService.bindObserver(things, 'thing', this.thing$, this.things$);
        //this._sailsEventService.bindObserver(sensors, 'sensor', this.sensor$, null, {addedTo: this.sensorAddedTo});
      },
      error => {

      },
      () => {
        this.startListeners()
      }
    )

    return this.things$.asObservable();
  }

  getSensor(id: number){
    return this.sensor$.asObservable().filter(sensor => sensor.id === id);
  }

  private loadSensors(things: Thing[]) {
    things.map(thing => {
      let sensors_array = [];
      thing.sensors.map(sensor => {
        sensors_array.push(sensor);
      })
      let ok = {
        thingId: thing.id,
        sensors$: new BehaviorSubject<any>(sensors_array),
        sensors: sensors_array
      }
      this.sensors.push(ok);
    })
  }

  private loadUnits(): Subject<Sensor>{
    let subject = new Subject<Sensor>();
    let count = this.sensors.length;
    this.sensors.map(
      sensor => {
        this._sailsService.get('/unit/'+sensor.lastMeasure.unit).subscribe(
          unit => {
            sensor.lastMeasure.unit = unit;
            subject.next(sensor);
            count--;
            if(count === 0){
              subject.complete();
            }
            return sensor;
          }
        )
      }
    )
    return subject;
  }






}
