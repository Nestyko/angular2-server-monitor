import { Sensor } from './sensor';

export interface Thing {
    id: number
    name?: string
    domain?: string
    ip?: string
    mac?: string
    alert?: boolean
    sensors: Array<Sensor>
    createdAt: Date
    updatedAt: Date
}
